<?php

include_once("initialize.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="../libs/jquery_jeditable/jquery.jeditable.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			$('.edit_area').editable('save.php', { 
				type       : 'textarea',
				loadurl    : '<?php echo $fname; ?>',
				cancel     : 'Cancel',
				submit     : 'Save',
				tooltip    : 'Click to edit...',
				onblur     : 'ignore',
				submitdata : {
					f: '<?php echo $fname; ?>',
					d: '<?php echo $dir; ?>'
				}
			});
		})

	</script>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<title><?php echo("$title"); ?></title>
</head>
<body>

	<nav>

		<?php

		// Display the existing files
		if ($handle = opendir($dir)) {
			echo "<h3>Files</h3>";

			echo "<ul>";

			while (false !== ($entry = readdir($handle))) {
				if (strstr($entry,'.md'))
				echo "<li><a href=\"./?f=".$entry."\">".$entry."</a></li>";
			}

			echo "</ul>";

			closedir($handle);
		}
		?>

		<a href="./?f=<?php echo base_convert(rand(1000000,10000000),10,36).'.md'; ?>">New file</a><br>

	</nav>


	<div class="content">
		<?php echo(date("F j, Y", $date) . ": $dir/$fname<br>"); ?>
		<div class="edit_area">
		<?php echo(Markdown(implode("", $file))); ?>
		</div>
	</div>
</body>
</html>
