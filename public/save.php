<?php

include_once("markdown.php");

$dir = ($_POST["d"]) ? $_POST["d"] : dirname(__FILE__);
$fname = ($_POST["f"]) ? $_POST["f"] : "index.md";

// Create a backup of the file we're overwriting
copy($dir.'/'.$fname,$dir.'/bak/'.$fname.'.bak.'.date('U'));

$the_file = fopen($dir.'/'.$fname,'w');
fwrite($the_file,$_POST['value']);
fclose($the_file);

$contents = file($dir.'/'.$fname);
echo(Markdown(implode("", $contents)));

?>
