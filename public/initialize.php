<?php 

	include_once("markdown.php");

	$dir = ($_GET["d"]) ? dirname(__FILE__) . "/" . $_GET["d"] : dirname(__FILE__);
	$fname = ($_GET["f"]) ? $_GET["f"] : "index.md";
	if (!file_exists("$dir/$fname")) {
		$new_file = fopen("$dir/$fname",'w');
		fwrite($new_file,"<!--\nTitle\n".date('Y/m/d')."\n-->\nThis is your new file.\n\nClick anywhere on this text to edit.");
		$file = file("$dir/$fname");
	} else {
		$file = file("$dir/$fname");
	}
	$title = $file[1];
	$date = strtotime($file[2]);

?>
